package com.hy.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HyRpcClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(HyRpcClientApplication.class, args);
    }

}
