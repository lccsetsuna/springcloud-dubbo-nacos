package com.hy.springboot.controller;

import com.hy.springboot.api.DemoService;
import com.hy.springboot.common.entity.Resp;
import com.hy.springboot.response.DemoResponse;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author codor
 * @date 2023/08/14 11:45
 */
@RestController
public class DemoController {


    @DubboReference(group = "${dubbo.consumer.group}", version = "${dubbo.consumer.version}")
    DemoService demoService;

    @RequestMapping
    public Resp<DemoResponse> access() {
        return demoService.getList(null);
    }
}
