package com.hy.springboot.api;

import com.hy.springboot.common.entity.Resp;
import com.hy.springboot.request.DemoRequest;
import com.hy.springboot.response.DemoResponse;

public interface DemoService {
    Resp<DemoResponse> getList(DemoRequest req);
}
