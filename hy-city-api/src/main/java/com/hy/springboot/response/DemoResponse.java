package com.hy.springboot.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class DemoResponse implements Serializable {
    private static final long serialVersionUID = 3842287925046504332L;
    private String username;
    private String sex;
}
