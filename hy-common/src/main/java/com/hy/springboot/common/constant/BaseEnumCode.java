package com.hy.springboot.common.constant;

public enum BaseEnumCode implements BaseEnumService {
	ERROR(10010, "请求发生未知异常"),
	SUCCESS(200, "OK");

	private String msg;
	private Integer code;

	private BaseEnumCode(Integer code, String msg) {
		this.msg = msg;
		this.code = code;
	}

	@Override
	public String getMsg() {
		return this.msg;
	}

		@Override
		public Integer getCode() {
			return this.code;
		}
}