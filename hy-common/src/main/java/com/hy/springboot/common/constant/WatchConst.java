

package com.hy.springboot.common.constant;

/**
 * Watch Const
 */
public class WatchConst {
    public final static String TCP = "tcp";
    public final static String _3G = "3G";
    public static final String LK_RESPONSE = "[3G*%s*0002*LK]";
    public static final String BTEMP2_RESPONSE = "[3G*%s*0006*btemp2]";
    public static final String BPHRT_RESPONSE = "[3G*%s*0005*bphrt]";



    public static final String BTEMP2_SEND_MSG = "[3G*%s*0009*bodytemp2]";
    public static final String HRT_SEND_MSG = "[3G*%s*000C*hrtstart,300]";


}
