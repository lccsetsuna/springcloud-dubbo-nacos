package com.hy.springboot.common.entity;

import com.hy.springboot.common.constant.BaseEnumCode;
import com.hy.springboot.common.constant.BaseEnumService;
import com.hy.springboot.common.utils.MDCUtils;

import java.io.Serializable;

/**
 * @author lccsetsun
 * @since 2024/2/1 11:28
 **/

public class Resp<T> implements Serializable {
	private static final long serialVersionUID = 1605409021477202585L;
	private Integer code;
	private String msg;
	private String traceId;
	private T data;

	public Resp() {
		this.traceId = MDCUtils.get();
	}

	public static <T> Resp<T> success(T data) {
		return (new RespBuilder()).code(BaseEnumCode.SUCCESS.getCode()).msg(BaseEnumCode.SUCCESS.getMsg()).data(data).build();
	}

	public static <T> Resp<T> success() {
		return (new RespBuilder()).code(BaseEnumCode.SUCCESS.getCode()).msg(BaseEnumCode.SUCCESS.getMsg()).build();
	}

	public static <T> Resp<T> fail() {
		return (new RespBuilder()).code(BaseEnumCode.ERROR.getCode()).msg(BaseEnumCode.ERROR.getMsg()).data((Object)null).build();
	}

	public static <T> Resp<T> fail(BaseEnumService e) {
		return (new RespBuilder()).code(e.getCode()).msg(e.getMsg()).data((Object)null).build();
	}

	public static <T> Resp<T> fail(String errMsg, Integer errCode) {
		return (new RespBuilder()).code(errCode).msg(errMsg).data((Object)null).build();
	}

	public static <T> Resp<T> fail(String errMsg) {
		return (new RespBuilder()).code(BaseEnumCode.ERROR.getCode()).msg(errMsg).data((Object)null).build();
	}

	public Integer getCode() {
		return this.code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return this.msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getTraceId() {
		return this.traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	public T getData() {
		return this.data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public Resp(Integer code, String msg, T data) {
		this.code = code;
		this.msg = msg;
		this.traceId = MDCUtils.get();
		this.data = data;
	}

	public static <T> RespBuilder<T> builder() {
		return new RespBuilder();
	}

	public static class RespBuilder<T> {
		private Integer code;
		private String msg;
		private T data;

		RespBuilder() {
		}

		public RespBuilder<T> code(final Integer code) {
			this.code = code;
			return this;
		}

		public RespBuilder<T> msg(final String msg) {
			this.msg = msg;
			return this;
		}

		public RespBuilder<T> data(final T data) {
			this.data = data;
			return this;
		}

		public Resp<T> build() {
			return new Resp(this.code, this.msg, this.data);
		}

		@Override
		public String toString() {
			return "Resp.RespBuilder(code=" + this.code + ", msg=" + this.msg + ", data=" + this.data + ")";
		}
	}
}
