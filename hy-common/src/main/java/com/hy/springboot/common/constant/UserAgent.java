package com.hy.springboot.common.constant;

/**
 * @author lccsetsun
 * @since 2024/1/31 11:57
 **/
public class UserAgent {
	public static final String USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HyService/1.0 (HY; HYSDK_HT 1.0.0) (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36";
}
