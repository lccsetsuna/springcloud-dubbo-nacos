package com.hy.springboot.common.constant;

public interface BaseIotEnumService {
	String getMsg();

	String getCode();
}
