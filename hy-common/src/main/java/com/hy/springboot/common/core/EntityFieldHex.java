package com.hy.springboot.common.core;




import com.hy.springboot.common.constant.EnumHexType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 定义注解类,用于注解某个类或方法
 *
 * @author Administrator
 */
@Target(ElementType.FIELD)//表示用于标识方法
@Retention(RetentionPolicy.RUNTIME)//表示运行时保留
public @interface EntityFieldHex {

    /**
     * 字段编号
     *
     * @return
     */
    String code() default "";

    /**
     * 字段名称
     *
     * @return
     */
    String name() default "";

    /**
     * 字段数值
     *
     * @return
     */
    String value() default "";

    /**
     * 字段描述
     *
     * @return
     */
    String intro() default "";

    /**
     * 字段分类
     *
     * @return
     */
    EnumHexType type() default EnumHexType.String;

    /**
     * 数据开始
     *
     * @return
     */
    int hexStart() default 0;

    /**
     * 数据长度
     *
     * @return
     */
    int hexLength() default 0;

    /**
     * 是否小端
     *
     * @return
     */
    int hexLb() default 0;

    /**
     * 数据缩放
     *
     * @return
     */
    double hexValueRate() default 1;

    /**
     * 数据加减
     *
     * @return
     */
    int hexValueRateAdd() default 0;

    /**
     * 数据小数位数
     *
     * @return
     */
    int hexValueDigit() default 3;

    /**
     * 数据位序
     *
     * @return
     */
    String hexValueFormat() default "";

    /**
     * 数据位序
     *
     * @return
     */
    int hexValueSeqno() default -1;

    /**
     * 数据选项
     *
     * @return
     */
    String hexValueOptions() default "";

}