package com.hy.springboot.common.utils;

/**
 * @author lccsetsun
 * @since 2024/4/16 14:54
 **/
public class GpsPointUtils {


	/**
	 * MethodName: calcLatAndlon
	 * Description:
	 *
	 * @date 2021/9/21 20:35
	 * @params: [posX 终点位置X轴的位置信息,posY 终点位置Y轴的位置信息,
	 * basePointLatitude 基点的GPS纬度坐标,
	 * azimuth 方位角(弧度),
	 * distance 2点之间的直线距离
	 * ]
	 * @author Tianjiao
	 */
	public static Double[] calcLatAndlon(double longitude, double latitude, double azimuth, double distance) {
		// 地球半径 单位米(m)
		double arc = 6371.393 * 1000;
//		// 终点的经度坐标
//		double lon = longitude + distance * Math.sin(azimuth) / (arc * Math.cos(latitude) * 2 * Math.PI / 360);
//		// 终点的纬度坐标
//		double lat = longitude + distance * Math.cos(azimuth) / (arc * 2 * Math.PI / 360);
		// 终点的经度坐标
		double lon = longitude + distance * Math.sin(azimuth) / (arc * Math.cos(longitude) * 2 * Math.PI / 360);
		// 终点的纬度坐标
		double lat = latitude + distance * Math.cos(azimuth) / (arc * 2 * Math.PI / 360);

		return new Double[]{lon,lat};
	}

	public static void main(String[] args) {
		Double lon = 115.7879121;
		Double lat = 32.8925;
		Double[] s = calcLatAndlon(lon,lat,19.51,30);
		System.out.println(s[0]+","+ s[1]);
	}


}
