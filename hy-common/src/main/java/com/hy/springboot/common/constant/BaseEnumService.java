package com.hy.springboot.common.constant;

public interface BaseEnumService {
	String getMsg();

	Integer getCode();
}
