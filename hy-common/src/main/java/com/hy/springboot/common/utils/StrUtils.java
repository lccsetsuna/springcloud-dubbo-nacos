package com.hy.springboot.common.utils;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author lccsetsun
 * @since 2024/1/31 10:57
 **/
public class StrUtils {

	public static String randomStringUpper(int length) {
		return randomString("abcdefghijklmnopqrstuvwxyz0123456789", length).toUpperCase();
	}

	private static String randomString(String baseString, int length) {
		if (isEmpty(baseString)) {
			return "";
		} else {
			StringBuilder sb = new StringBuilder(length);
			if (length < 1) {
				length = 1;
			}

			int baseLength = baseString.length();

			for(int i = 0; i < length; ++i) {
				int number = randomInt(baseLength);
				sb.append(baseString.charAt(number));
			}

			return sb.toString();
		}
	}

	private static int randomInt(int limit) {
		return getRandom().nextInt(limit);
	}

	private static ThreadLocalRandom getRandom() {
		return ThreadLocalRandom.current();
	}

	public static boolean isEmpty(CharSequence str) {
		return str == null || str.length() == 0;
	}
	public static boolean isBlank(CharSequence str) {
		int length;
		if (str != null && (length = str.length()) != 0) {
			for(int i = 0; i < length; ++i) {
				if (!isBlankChar(str.charAt(i))) {
					return false;
				}
			}

			return true;
		} else {
			return true;
		}
	}
	private static boolean isBlankChar(int c) {
		return Character.isWhitespace(c) || Character.isSpaceChar(c) || c == 65279 || c == 8234;
	}
}
