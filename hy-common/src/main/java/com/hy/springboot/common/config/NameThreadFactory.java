package com.hy.springboot.common.config;

import com.hy.springboot.common.utils.StrUtils;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author lccsetsun
 * @since 2024/2/3 13:57
 **/
public class NameThreadFactory implements ThreadFactory {
	private final String prefix;
	private final AtomicInteger threadNumber;

	public NameThreadFactory(String prefix) {
		this.threadNumber = new AtomicInteger(1);
		this.prefix = StrUtils.isBlank(prefix) ? "hySync" : prefix;
	}
	@Override
	public Thread newThread(Runnable r) {
		Thread t = new Thread(null, r, this.prefix+this.threadNumber.getAndIncrement());
		return t;
	}
}
