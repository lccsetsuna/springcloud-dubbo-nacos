package com.hy.springboot.common.constant;

public enum EnumHexType {

    Int("整数", 1),
    Int1("整数带负", 2),
    Float("浮点", 3),
    Date("日期", 4),
    /**
     * 16进制序列化字符串
     */
    String("字符串", 10),
    /**
     * hex -> ascii string
     */
    StringAscii("字符串Ascii", 11),
    String2("字符串二进制", 12),
    ListString2("集合字符串-字符串集合(前2位是长度后面是数据)", 20),
    ListString4("集合字符串-字符串集合(前4位是长度后面是数据)", 21);

    // 成员变量
    private String name;
    private int index;

    // 构造方法
    private EnumHexType(String name, int index) {
        this.name = name;
        this.index = index;
    }

    //覆盖方法
    @Override
    public String toString() {
        return this.index + "_" + this.name;
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }
}
