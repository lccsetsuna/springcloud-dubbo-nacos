package com.hy.springboot.common.utils;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lccsetsun
 * @since 2024/2/29 15:38
 **/
public class Point2DUtils {

	/**
	 * 地球半径(米)
	 */
	private static final double EARTH_RADIUS = 6378137.0;

	private static double rad(double d) {
		return d * Math.PI / 180.0;
	}

	/**
	 *  0.0  0.50
	 *  25 . 55
	 *  50.0 50.50
	 *
	 *
	 * @param args
	 */

	public static void main(String[] args) {
		Point2D.Double d = new Point2D.Double(51,23);
		Point2D.Double d2 = new Point2D.Double(58,25);

		List<Point2D.Double> points = new ArrayList<>(); // 存放所有点的列表
		Point2D.Double p1 = new Point2D.Double(0, 0);
		Point2D.Double p2 = new Point2D.Double(50, 0);
		Point2D.Double p22 = new Point2D.Double(60, 25);
		Point2D.Double p3 = new Point2D.Double(50, 50);
		Point2D.Double p4 = new Point2D.Double(0, 50);

		points.add(p1);
		points.add(p2);
		points.add(p22);
		points.add(p3);
		points.add(p4);

		System.out.println(isPointInPolygon(d,points));
		System.out.println(isPointInPolygon(d2,points));
	}

	public static boolean isPointInPolygon(Point2D.Double point, List<Point2D.Double> polygon) {
		int count = 0;
		int size = polygon.size();
		for (int i = 0; i < size; i++) {
			Point2D.Double p1 = polygon.get(i);
			Point2D.Double p2 = polygon.get((i + 1) % size);
			if (p1.y == p2.y) {
				continue;
			}
			if (point.y < Math.min(p1.y, p2.y)) {
				continue;
			}
			if (point.y >= Math.max(p1.y, p2.y)) {
				continue;
			}
			double x = (point.y - p1.y) * (p2.x - p1.x) / (p2.y - p1.y) + p1.x;
			if (x > point.x) {
				count++;
			} else if (x == point.x) {
				return true;
			}
		}
		return count % 2 == 1;
	}
}