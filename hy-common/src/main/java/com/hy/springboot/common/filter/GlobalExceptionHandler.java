package com.hy.springboot.common.filter;

import com.hy.springboot.common.entity.Resp;
import com.hy.springboot.common.exception.BizException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.AccessDeniedException;
/**
 * @author lccsetsun
 * @since 2024/2/1 17:04
 **/

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

	/**
	 * 权限校验异常
	 */
	@ExceptionHandler(AccessDeniedException.class)
	public Resp handleAccessDeniedException(AccessDeniedException e, HttpServletRequest request)
	{
		log.error("URL {} request exception msg {}",request.getRequestURL().toString(),e.getMessage());
		return Resp.fail("没有权限，请联系管理员授权",HttpStatus.FORBIDDEN.value());
	}

	/**
	 * 请求方式不支持
	 */
	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	public Resp handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException e,
														  HttpServletRequest request)
	{
		log.error("URL {} request exception msg {}",request.getRequestURL().toString(),e.getMessage());
		return Resp.fail(e.getMessage());
	}

	/**
	 * 请求路径中缺少必需的路径变量
	 */
	@ExceptionHandler(MissingPathVariableException.class)
	public Resp handleMissingPathVariableException(MissingPathVariableException e, HttpServletRequest request)
	{
		log.error("URL {} request exception msg {}",request.getRequestURL().toString(),e.getMessage());
		return Resp.fail(String.format("请求路径中缺少必需的路径变量[%s]", e.getVariableName()));
	}

	/**
	 * 请求参数类型不匹配
	 */
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public Resp handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e, HttpServletRequest request)
	{
		log.error("URL {} request exception msg {}",request.getRequestURL().toString(),e.getMessage());
		return Resp.fail(String.format("请求参数类型不匹配，参数[%s]要求类型为：'%s'，但输入值为：'%s'", e.getName(), e.getRequiredType().getName(), e.getValue()));
	}

	/**
	 * 拦截未知的运行时异常
	 */
	@ExceptionHandler(RuntimeException.class)
	public Resp handleRuntimeException(RuntimeException e, HttpServletRequest request)
	{
		log.error("URL {} request exception msg {}",request.getRequestURL().toString(),e.getMessage());
		return Resp.fail(e.getMessage());
	}

	/**
	 * 系统异常
	 */
	@ExceptionHandler(Exception.class)
	public Resp handleException(Exception e, HttpServletRequest request)
	{

		log.error("URL {} request exception msg {}",request.getRequestURL().toString(),e.getMessage());
		return Resp.fail(e.getMessage());
	}

	/**
	 * 系统异常
	 */
	@ExceptionHandler(BizException.class)
	public Resp handleException(BizException e, HttpServletRequest request)
	{

		log.error("URL {} request exception msg {}",request.getRequestURL().toString(),e.getMessage());
		return Resp.fail(e.getErrMsg(),e.getCode());
	}
	
}
