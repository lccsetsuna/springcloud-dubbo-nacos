package com.hy.springboot.common.utils;

import com.hy.springboot.common.constant.Constant;
import org.slf4j.MDC;

import java.util.Map;

/**
 * @author lccsetsun
 * @since 2024/1/31 10:46
 **/
public class MDCUtils {
	public MDCUtils() {
	}

	public static void set() {
		MDC.put(Constant.TRACE_ID, StrUtils.randomStringUpper(32));
	}

	public static void set(String requestId) {
		MDC.put(Constant.TRACE_ID, requestId);
	}

	public static String get() {
		return MDC.get(Constant.TRACE_ID);
	}

	public static void clear() {
		MDC.clear();
	}



	public static void main(String[] args) {
		System.out.println(StrUtils.randomStringUpper(32));
	}

	public static void setContextMap(Map<String, String> context) {
		MDC.setContextMap(context);
	}
}
