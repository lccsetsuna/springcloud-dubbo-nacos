package com.hy.springboot.common.exception;

public interface BaseExceptionService {
	String getErrMsg();

	Integer getCode();

}
