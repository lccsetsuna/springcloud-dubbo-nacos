package com.hy.springboot.common.exception;

/**
 * @author lccsetsun
 * @since 2024/2/1 17:11
 **/
public class BizException extends RuntimeException{

	private Integer code;
	private String errMsg;

	public BizException() {
		super();
	}

	public BizException(String message) {
		super(message);
	}

	public BizException(BaseExceptionService e) {
		super("error code: "+e.getCode()+" \n errorMsg: "+e.getErrMsg());
		this.code = e.getCode();
		this.errMsg = e.getErrMsg();
	}

	public Integer getCode() {
		return code;
	}

	public String getErrMsg() {
		return errMsg;
	}
}
