package com.hy.springboot.common.exception;

public enum BaseExceptionErrorEnum implements BaseExceptionService{
	ERROR_BASE(401,"未知的异常");

	private String errMsg;
	private Integer code;

	BaseExceptionErrorEnum(Integer code,String errMsg) {
		this.errMsg = errMsg;
		this.code = code;
	}

	@Override
	public String getErrMsg() {
		return this.errMsg;
	}

	@Override
	public Integer getCode() {
		return this.code;
	}
}
