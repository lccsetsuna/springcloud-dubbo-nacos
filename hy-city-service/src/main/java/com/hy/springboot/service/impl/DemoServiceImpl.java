package com.hy.springboot.service.impl;

import com.hy.springboot.api.DemoService;
import com.hy.springboot.common.entity.Resp;
import com.hy.springboot.common.utils.DateUtils;
import com.hy.springboot.request.DemoRequest;
import com.hy.springboot.response.DemoResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;

@Slf4j
@DubboService(interfaceClass = DemoService.class,group = "${dubbo.consumer.group}",version = "${dubbo.consumer.version}")
public class DemoServiceImpl implements DemoService {
    @Override
    public Resp<DemoResponse> getList(DemoRequest req) {
        DemoResponse demoResponse = new DemoResponse();
        demoResponse.setSex("F");
        demoResponse.setUsername("lcc");
        log.info(" response: {} time {}", demoResponse, DateUtils.getDate());
        return Resp.success(demoResponse);
    }
}
