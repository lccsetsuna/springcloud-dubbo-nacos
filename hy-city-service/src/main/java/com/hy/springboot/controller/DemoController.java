package com.hy.springboot.controller;

import com.hy.springboot.api.DemoService;
import com.hy.springboot.common.entity.Resp;
import com.hy.springboot.response.DemoResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Slf4j
@RestController
@RequestMapping("t")
public class DemoController {

    @Resource
    private DemoService demoService;

    @GetMapping("s")
    @ResponseBody
    public Resp<DemoResponse> getDemoService() {
        log.info("getDemoService");
        return demoService.getList(null);
    }
}
