package com.hy.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HyRpcServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(HyRpcServiceApplication.class, args);
    }

}
