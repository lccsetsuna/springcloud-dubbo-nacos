# springcloud-dubbo-nacos

#### 介绍
springcloud 集成nacos dubbo 自研全链路ID(tranceId) demo级项目 可供新手进行学习
time: 2024-04-23

#### 软件架构
```
springcloud-dubbo-nacos 根目录
    |-- start service服务 启动类
    |-- hy-city-service 具体业务
    |-- hy-city-api 对外提供的api可供RPC调用
    |-- hy-common 公共类
    |-- hy-client-demo 消费者,可调用服务者(provider)进行测试链路ID
```


#### 安装教程

1.  依赖 nacos 具体安装可参考 docker nacos 快速部署测试

#### 使用说明

1.  启动nacos->启动start 服务端->启动demo-client
2. 访问 http://localhost:9001/ 
```json
{"code":200,"msg":"OK","traceId":"BQ5K9VVGUW9BRK85H810UYRP4S0SNCMX","data":{"username":"lcc","sex":"F"}}
```


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


## Star History

[![Star History Chart](https://api.star-history.com/svg?repos=夕阳/springcloud-dubbo-nacos&type=Date)](https://star-history.com/#%E5%A4%95%E9%98%B3/springcloud-dubbo-nacos&Date)
